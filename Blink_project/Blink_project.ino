void setup()
{
	// initialize digital pin 13 as an output.
	pinMode(13, OUTPUT);
	Serial.begin(115200);
	int count = 0;
}

// the loop function runs over and over again forever
void loop() {
	int count = 0;
	unsigned long last_time = millis();
	while (true)
	{
		count++;
		digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
		delay(250);              // wait for a second
		Serial.print("Counter is :");
		Serial.println(count);
		digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
		delay(250);              // wait for a second
	}
}
