#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>
#include <DHT.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define RelayPin 4

#define DS1820PINDATA 5
#define DHTPINDATA 3
#define DHTPINVCC  2
#define DHTTYPE DHT22
float temperature;
float humidity;
DHT dht(DHTPINDATA, DHTTYPE);
// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire ds(DS1820PINDATA);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&ds);
// arrays to hold device address
DeviceAddress insideThermometer;

RF24 radio(9, 10);                   // nRF24L01(+) radio attached using Getting Started board

RF24Network network(radio);          // Network uses that radio

const uint16_t this_node = 01;        // Address of our node in Octal format
const uint16_t other_node = 00;       // Address of the other node in Octal format

const unsigned long interval = 15000; //ms  // How often to send 'hello world to the other unit

unsigned long last_sent;             // When did we last send?
unsigned long packets_sent;          // How many have we sent already
float dscelsius;

struct payload_t {                  // Structure of our payload
  unsigned long ms;
  unsigned long counter;
  float DHT_temperature;
  float DHT_humidity;
  float DS_temperature;
  float temperature_setpoint;
};

void setup(void)
{
  Serial.begin(57600);
  Serial.println("Temperature control program test ");

  SPI.begin();
  radio.begin();
  network.begin(/*channel*/ 90, /*node address*/ this_node);
  pinMode(RelayPin, OUTPUT);
  //  digitalWrite(RelayPin, HIGH);
  //  delay(500);
  digitalWrite(RelayPin, LOW);
  pinMode(DHTPINVCC, OUTPUT);
  //make it blink at startup
  digitalWrite(DHTPINVCC, HIGH);

  Serial.print("Locating One wire devices...");
  sensors.begin();
  Serial.print("Found ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.println(" device(s).");

  // report parasite power requirements
  Serial.print("Parasite power is: ");
  if (sensors.isParasitePowerMode()) Serial.println("ON");
  else Serial.println("OFF");

  // assign address manually.  the addresses below will beed to be changed
  // to valid device addresses on your bus.  device address can be retrieved
  // by using either oneWire.search(deviceAddress) or individually via
  // sensors.getAddress(deviceAddress, index)
  //insideThermometer = { 0x28, 0x1D, 0x39, 0x31, 0x2, 0x0, 0x0, 0xF0 };

  // Method 1:
  // search for devices on the bus and assign based on an index.  ideally,
  // you would do this to initially discover addresses on the bus and then
  // use those addresses and manually assign them (see above) once you know
  // the devices on your bus (and assuming they don't change).
  if (!sensors.getAddress(insideThermometer, 0)) Serial.println("Unable to find address for Device 0");

  // method 2: search()
  // search() looks for the next device. Returns 1 if a new address has been
  // returned. A zero might mean that the bus is shorted, there are no devices,
  // or you have already retrieved all of them.  It might be a good idea to
  // check the CRC to make sure you didn't get garbage.  The order is
  // deterministic. You will always get the same devices in the same order
  //
  // Must be called before search()
  //oneWire.reset_search();
  // assigns the first address found to insideThermometer
  //if (!oneWire.search(insideThermometer)) Serial.println("Unable to find address for insideThermometer");

  // show the addresses we found on the bus
  Serial.print("Device 0 Address: ");
  printAddress(insideThermometer);
  Serial.println();

  // set the resolution to 9 bit (Each Dallas/Maxim device is capable of several different resolutions)
  sensors.setResolution(insideThermometer, 12);

  Serial.print("Device 0 Resolution: ");
  Serial.print(sensors.getResolution(insideThermometer), DEC);
  Serial.println();
}

void loop() {

  network.update();                          // Check the network regularly


  unsigned long now = millis();              // If it's time to send a message, send it!
  if ( now - last_sent >= interval  )
  {
    last_sent = now;

    Serial.print("Reading...");
    readDHTsensor();
    sensors.requestTemperatures();
    GetTemperature(insideThermometer);
    //    readDSsensor();
    Serial.print("Sending...");
    payload_t payload = { millis(), packets_sent++, temperature, humidity, dscelsius, 61.5};
    RF24NetworkHeader header(/*to node*/ other_node);
    bool ok = network.write(header, &payload, sizeof(payload));
    if (ok)
    {
      Serial.println("ok.");
      if ( packets_sent % 2 == 0 )      {
        digitalWrite(RelayPin, LOW);}
      else      {
        digitalWrite(RelayPin, HIGH);      } }
    else{
      Serial.println("failed.");}
  }
      delay(2);
    network.update();                  // Check the network regularly
    while ( network.available() ) {     // Is there anything ready for us?
      RF24NetworkHeader header;        // If so, grab it and print it out
      payload_t payload;
      network.read(header, &payload, sizeof(payload));
      Serial.print("Received payload # ");
      Serial.print(payload.counter);
      Serial.print(" at ");
      Serial.print(payload.ms);
      Serial.print(",\nDHT(T:");
      Serial.print(payload.DHT_temperature);
      Serial.print("c,H:");
      Serial.print(payload.DHT_humidity);
      Serial.print("p), DS(T:");
      Serial.print(payload.DS_temperature);
      Serial.print("c), \nSetPoint(T:");
      Serial.print(payload.temperature_setpoint);
      Serial.print("c),size:");
      Serial.print(sizeof(payload));
      Serial.print("\n");
    }
  delay(2);
  
}


void readDHTsensor() {
  temperature = dht.readTemperature();
  humidity = dht.readHumidity();
  printf("Got reading,\n\rTemperatuer: %4.2f, Humidity: %4.2f\n\r", temperature, humidity);
}

void GetTemperature(DeviceAddress deviceAddress)
{
  // method 1 - slower
  //Serial.print("Temp C: ");
  //Serial.print(sensors.getTempC(deviceAddress));
  //Serial.print(" Temp F: ");
  //Serial.print(sensors.getTempF(deviceAddress)); // Makes a second call to getTempC and then converts to Fahrenheit

  // method 2 - faster
  dscelsius = sensors.getTempC(deviceAddress);
  Serial.print("Temp C: ");
  Serial.print(dscelsius);
  //  Serial.print(" Temp F: ");
  //  Serial.println(DallasTemperature::toFahrenheit(tempC)); // Converts tempC to Fahrenheit
}

void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}
