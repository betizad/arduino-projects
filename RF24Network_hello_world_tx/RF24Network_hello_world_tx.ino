/*
 Copyright (C) 2012 James Coliz, Jr. <maniacbug@ymail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.

 Update 2014 - TMRh20
 */

/**
 * Simplest possible example of using RF24Network
 *
 * TRANSMITTER NODE
 * Every 2 seconds, send a payload to the receiver node.
 */

#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>
#include <DHT.h>
#include <OneWire.h>

#define RelayPin 4

#define DS1820PINDATA 5
#define DHTPINDATA 3
#define DHTPINVCC  2
#define DHTTYPE DHT22
float temperature;
float humidity;
DHT dht(DHTPINDATA, DHTTYPE);
OneWire ds(DS1820PINDATA);

RF24 radio(9, 10);                   // nRF24L01(+) radio attached using Getting Started board

RF24Network network(radio);          // Network uses that radio

const uint16_t this_node = 01;        // Address of our node in Octal format
const uint16_t other_node = 00;       // Address of the other node in Octal format

const unsigned long interval = 5000; //ms  // How often to send 'hello world to the other unit

unsigned long last_sent;             // When did we last send?
unsigned long packets_sent;          // How many have we sent already
float dscelsius;

struct payload_t {                  // Structure of our payload
  unsigned long ms;
  unsigned long counter;
  float DHT_temperature;
  float DHT_humidity;
  float DS_temperature;
  float temperature_setpoint;
  float time_setpoint;
};

void setup(void)
{
  Serial.begin(57600);
  Serial.println("RF24Network/examples/helloworld_tx/");

  SPI.begin();
  radio.begin();
  network.begin(/*channel*/ 90, /*node address*/ this_node);
  pinMode(RelayPin, OUTPUT);
  //  digitalWrite(RelayPin, HIGH);
  //  delay(500);
  //  digitalWrite(RelayPin, LOW);
  pinMode(DHTPINVCC, OUTPUT);
  //make it blink at startup
  digitalWrite(DHTPINVCC, HIGH);
}

void loop() {

  network.update();                          // Check the network regularly


  unsigned long now = millis();              // If it's time to send a message, send it!
  if ( now - last_sent >= interval  )
  {
    last_sent = now;

    Serial.print("Reading...");
    readDHTsensor();
    readDSsensor();
    Serial.print("Sending...");
    payload_t payload = { millis(), packets_sent++, temperature, humidity, dscelsius, 61.5, 1.0};
    RF24NetworkHeader header(/*to node*/ other_node);
    bool ok = network.write(header, &payload, sizeof(payload));
    if (ok)
    {
      Serial.println("ok.");
      if ( packets_sent % 2 == 0 )
      {
        digitalWrite(RelayPin, LOW);
      }
      else
      {
        digitalWrite(RelayPin, HIGH);
      }
    }
    else
    {
      Serial.println("failed.");
    }
  }
}


void readDHTsensor() {
  temperature = dht.readTemperature();
  humidity = dht.readHumidity();
  printf("Got reading,\n\rTemperatuer: %4.2f, Humidity: %4.2f\n\r", temperature, humidity);
}

void readDSsensor() {
  byte i;
  //  byte present = 0;
  //  byte type_s;
  byte data[12];

  ds.reset();      // Reset the OneWire bus in preparation for communication
  ds.skip();       // Skip addressing, since there is only one sensor
  ds.write(0x44);  // Send 44, the conversion command

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  // Wait for the conversion to complete
  // Read back the data
  ds.reset();                      // Reset the OneWire bus in preparation for communication
  ds.skip();                       // Skip addressing, since there is only one sensor
  ds.write(0xBE);                  // Send the "Read Scratchpad" command
  for ( i = 0; i < 9; i++) {
    data[i] = ds.read();    // Read the 9 bytes into data[]
  }
  //
  //  // Convert the data to actual temperature
  //  // because the result is a 16 bit signed integer, it should
  //  // be stored to an "int16_t" type, which is always 16 bits
  //  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  dscelsius = (float)raw / 16.0;
//  fahrenheit = celsius * 1.8 + 32.0;
  Serial.print("  Temperature = ");
  Serial.print(dscelsius);
  Serial.print(" Celsius \r\n");
//  Serial.print(fahrenheit);
//  Serial.println(" Fahrenheit");
}
